const express = require('express')
const mongoose = require('mongoose')
const url = 'mongodb+srv://admin:admin1234@cluster0.ujm7t.mongodb.net/userApi?retryWrites=true&w=majority'

const app = express();

mongoose.connect(url , {useNewUrlParser: true})

const con = mongoose.connection

con.on('open', function (){
    console.log('connected to db');
})

app.use(express.json())

const routes = require('./routes/user')
const postRoute = require('./routes/post')
app.use('/user', routes)
app.use('/post', postRoute)

app.listen(process.env.PORT || 3000 , function (){
    console.log('Listening on 3000');
})
