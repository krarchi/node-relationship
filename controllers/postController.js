const mongoose = require('mongoose')
const post = require('../models/posts')
const user = require('../models/users')

module.exports = {
    create: async (req, res) => {
        try {
            const data = req.body
            const unsavedPost = new post({
                text: data.text,
            })
            const userById = await user.findById(mongoose.mongo.ObjectId(data.user));
            unsavedPost.user = userById
            await unsavedPost.save()
            userById.posts.push(unsavedPost)
            await userById.save()
            res.status(200).send('Post Created')
        } catch (err) {
            res.send(err)
        }
    },
    list: async (req, res) => {
        try {
            const posts = await post.find()
            res.json(posts)
        } catch (err) {
            res.send(err)
        }
    },
    like: async (req, res) => {
        try{
            const data = req.body
            const searchedPost = await post.findById(mongoose.mongo.ObjectId(data.post))
            const searchedUser = await user.findById(mongoose.mongo.ObjectId(data.user));
            searchedPost.likes.push(searchedUser)
            searchedUser.liked.push(searchedPost)
            await searchedUser.save()
            await searchedPost.save()
            res.json({status: 200, message: 'Like Successfull'})
        }catch (err){
            res.json(err)
        }

    },
    listUser: async (req, res) => {
        const searchedPost = await post.findById(mongoose.mongo.ObjectId(req.params.postId)).populate('likes','name username')
        res.json(searchedPost.likes)
    }
}
