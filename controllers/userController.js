const user = require('../models/users')

module.exports = {
    create: async (req, res) => {
        const data = req.body;
        let password = data.password;
        let buff = new Buffer(password);
        let encodedPass = buff.toString('base64');
        const userd = new user({
            name: data.name,
            username: data.username,
            password: encodedPass,
            status: data.status,
        })
        try {
            const check = await user.find({username: data.username})
            if (check.length === 0) {
                await userd.save();
                res.status(200).send('User Created Successfully')
            } else {
                res.status(406).send("Username already Taken")
            }
        } catch (err) {
            res.status(406).send(err);
        }
    },
    list: async (req, res) => {
        try {
            const userd = await user.find()
            res.json(userd)
        } catch (err) {
            res.send(err)
        }
    },
    login: async (req, res) => {
        const data = req.body;
        const searchedUser = await user.find({username: data.username})
        if (searchedUser.length !== 0) {
            let buff = new Buffer(searchedUser[0].password, 'base64');
            let decodedPass = buff.toString('ascii');
            if (data.password === decodedPass) {
                res.status(200).send('Logged in successfully ')
            } else {
                res.status(401).send('Login Error')
            }
        } else {
            res.status(404).send('User Not found')
        }
    },
    liked: async (req, res) => {
        try {
            const userd = await user.find({username: req.params.username}).populate('liked', 'text')
            res.json(userd[0].liked)
        } catch (err) {
            res.send(err)
        }

    },
    listPosts: async (req, res) => {
        try {
            const userd = await user.find({username: req.params.username}).populate('posts')
            res.json(userd[0].posts)
        } catch (err) {
            res.send(err)
        }

    }
}
