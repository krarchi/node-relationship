const express = require('express');
const router = express.Router();
const controller = require('/controllers/userController')


router.get('/users', controller.list);

router.post('/create', controller.create);

router.post('/login', controller.login)

router.get('/getPosts/:username', controller.listPosts )

router.get('/liked/:username', controller.liked)

module.exports = router
