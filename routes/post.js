const express = require('express');
const router = express.Router();
const controller = require('/controllers/postController')


router.post('/create', controller.create)

router.get('/posts', controller.list)

router.post('/like', controller.like)

router.get('/usersLiked/:postId', controller.listUser)

module.exports = router
