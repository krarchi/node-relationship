const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    status: {
        type: String,
        default: ''
    },
    posts: [{
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'post'
    }],
    liked: [
        {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'post'
        }
    ]

})

module.exports = mongoose.model('user', userSchema)
