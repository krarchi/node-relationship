const mongoose = require('mongoose')

const postSchema = new mongoose.Schema({
    text: {
        type: String,
        required: true
    },
    user: {
        type: mongoose.SchemaTypes.ObjectID,
        ref: 'user'
    },
    likes: [
        {
            type: mongoose.SchemaTypes.ObjectID,
            ref: 'user'
        }
    ]
})

module.exports = mongoose.model('post', postSchema)
